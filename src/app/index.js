import React from 'react';
import Screen from '../screen';
import Keyboard from "../keyboard";
import './style.scss';

export default class App extends React.Component {

constructor(){
  super();
  this.state = {
    displayValue: "0",
    watingForOperator:false,
    value: "0",
    result:"0",
    operator:"",
    displayCount: "0",
    keepValue: false,
    acum: 0,
    priorityNumber: 0
  }
  this.inputDigit = this.inputDigit.bind(this);
}

// divOnChange(){
//   const { displayCount } = this.state
//   const { result } = "0"

//   document.getElementById("changingDisplay").change(() => {
//     console.log("hola")
//   })
// }





inputDigit(digit){
  const { displayValue } = this.state
  const { watingForOperator } = false
  const { value } = "0"
  const { result } = "0"
  const { operator } = ""
  const { displayCount } = this.state
  const { keepValue } = false
  const { acum } = 0
  const { priorityNumber }= 0
  
  if(digit === "."){
    if(displayValue.indexOf(".") === -1){
      this.setState({
        displayValue: displayValue + "."
        })
      }
  }else if(digit === "C"){
    this.setState({
      displayValue: "0",
      result: 0,
      displayCount: "0",
      value: "0",
      keepValue: false,
      acum:0,
      watingForOperator: false,
      operator: "",
      priorityNumber:0
    })
  }else if(digit === "DE"){
    this.setState({
      displayCount: displayCount.slice(0,-1),
      
    })
  }else if(this.state.watingForOperator && (digit === "=")){
    if(this.state.operator === "x"){
      const num = String(Number(this.state.value * this.state.displayValue))
      if(num === "NaN"){
        this.setState({
          result: "Syntax Error",
          watingForOperator: false,
          displayValue: "0"
        })
      }else{
        this.setState({
          result: num,
          watingForOperator: false,
          displayValue: "0"
        })
      }
    }else if(this.state.operator === "÷"){
      if((displayValue === "0") && (Number(this.state.value) < 0)) {
        this.setState({
          watingForOperator: false,
          result: "-∞",
          displayValue: "0"
        })
      }else if((displayValue === "0") && this.state.value === "0"){
        this.setState({
          watingForOperator: false,
          result: "Indetermination",
          displayValue:"0"
        })
      }else if(displayValue ==="0"){
        this.setState({
          watingForOperator: false,
          result: "∞",
          displayValue:"0"
        })
      }else{
        const num = String(Number(this.state.value / this.state.displayValue))
        if( num === "NaN"){
          this.setState({
            result: "Syntax Error",
            watingForOperator: false,
            displayValue: "0"
          })
        }else{
          this.setState({
            result: num,
            watingForOperator: false,
            displayValue: "0"
          })
        }
      }
    }else if(this.state.operator === "+"){
      const num = String(Number(this.state.value) + Number(this.state.displayValue))
      if( num === "NaN"){
        this.setState({
          result: "Syntax Error",
          watingForOperator: false,
          displayValue: "0"
        })
      }else{
        this.setState({
          result: num,
          watingForOperator: false,
          displayValue: "0"
        })
      }
    }else if(this.state.operator === "-" ){
      const num = String(Number(this.state.value - this.state.displayValue))
      if(num === "NaN"){
        this.setState({
          result: "Syntax Error",
          watingForOperator: false,
          displayValue: "0"
        })
      }else{
        this.setState({
          result: num,
          watingForOperator: false,
          displayValue: "0"
        })
      }
    }
  }else if(digit === "x"){
    if (this.state.keepValue === true ){
      if(this.state.operator === "-"){
        this.setState({
          displayCount: displayCount=== "0" ? String(digit) : displayCount + digit,
          watingForOperator:true,
          keepValue: true,
          value: this.state.result,
          displayValue:"0",
          operator:"x", 
          priorityNumber: -Number(displayValue)
        })
      }else{
        this.setState({
          displayCount: displayCount=== "0" ? String(digit) : displayCount + digit,
          watingForOperator:true,
          keepValue: true,
          value: this.state.result,
          displayValue:"0",
          operator:"x", 
          priorityNumber: Number(displayValue)
        })
      }
    }else{this.setState({
        displayCount: displayCount=== "0" ? String(digit) : displayCount + digit,
        watingForOperator:true,
        keepValue: true,
        value: displayValue,
        displayValue:"0",
        operator:"x",
      })
    }
  }else if(digit === "÷"){
    if (this.state.keepValue === true ){
      if(this.state.operator === "-"){
        this.setState({
          displayCount: displayCount=== "0" ? String(digit) : displayCount + digit,
          watingForOperator:true,
          keepValue: true,
          value: this.state.result,
          displayValue:"0",
          operator:"÷", 
          priorityNumber: -Number(displayValue)
        })
      }else{
        this.setState({
          displayCount: displayCount=== "0" ? String(digit) : displayCount + digit,
          watingForOperator:true,
          keepValue: true,
          value: this.state.result,
          displayValue:"0",
          operator:"÷", 
          priorityNumber: Number(displayValue)
        })
      }
    }else{this.setState({
        displayCount: displayCount=== "0" ? String(digit) : displayCount + digit,
        watingForOperator:true,
        keepValue: true,
        value: displayValue,
        displayValue:"0",
        operator:"÷",
      })
    }
  }else if(digit === "-"){
    if (this.state.keepValue === true ){
      this.setState({
        displayCount: displayCount=== "0" ? String(digit) : displayCount + digit,
        watingForOperator:true,
        keepValue: true,
        value: this.state.result,
        displayValue:"0",
        operator:"-", 
      })
    }else{this.setState({
        displayCount: displayCount=== "0" ? String(digit) : displayCount + digit,
        watingForOperator:true,
        keepValue: true,
        value: displayValue,
        displayValue:"0",
        operator:"-",
      })
    }
  }else if(digit === "+"){
    if (this.state.keepValue === true ){
      this.setState({
        displayCount: displayCount=== "0" ? String(digit) : displayCount + digit,
        watingForOperator:true,
        keepValue: true,
        value: this.state.result,
        displayValue:"0",
        operator:"+",
        
      })
    }else{this.setState({
        displayCount: displayCount=== "0" ? String(digit) : displayCount + digit,
        watingForOperator:true,
        keepValue: true,
        value: displayValue,
        displayValue:"0",
        operator:"+",
        
      })
    }
  }else if(digit != "="){
    if(this.state.operator === "+"){
      this.setState({
        displayValue: displayValue === "0" ? String(digit) : displayValue + digit,
        displayCount: displayCount === "0" ? String(digit) : displayCount + digit,
        result: Number(this.state.value) + Number(displayValue + digit)
      })
    }else if(this.state.operator === "-"){
      this.setState({
        displayValue: displayValue === "0" ? String(digit) : displayValue + digit,
        displayCount: displayCount === "0" ? String(digit) : displayCount + digit,
        result: Number(this.state.value) - Number(displayValue + digit)
      })
    }else if(this.state.operator === "x"){
      this.setState({
        displayValue: displayValue === "0" ? String(digit) : displayValue + digit,
        displayCount: displayCount === "0" ? String(digit) : displayCount + digit,
        result: Number(this.state.priorityNumber) * Number(displayValue + digit) + Number(this.state.value) -Number(this.state.priorityNumber),
      })
    }else if(this.state.operator === "÷"){
      this.setState({
        displayValue: displayValue === "0" ? String(digit) : displayValue + digit,
        displayCount: displayCount === "0" ? String(digit) : displayCount + digit,
        result: Number(this.state.priorityNumber) / Number(displayValue + digit) + Number(this.state.value) -Number(this.state.priorityNumber)
      })
    }else{
      this.setState({
        displayValue: displayValue === "0" ? String(digit) : displayValue + digit,
        displayCount: displayCount === "0" ? String(digit) : displayCount + digit,
        result: Number(displayValue + digit)
      })
    } 
  }
}


  render() {

    return (
      <div className="container-fluid calculator">
        <div className="row blackcontainer">
          <Screen displayCount = {this.state.displayCount} result= {this.state.result} />
        </div>
        <Keyboard inputDigit={this.inputDigit} />
      </div>
    )
  }
}