import React from 'react';
import './style.scss'

export default class Button extends React.Component {
	constructor(props){
		super(props)
		
	}
	
	render(){
		return(
			<div>
				<button className={this.props.type} onClick={() => this.props.inputDigit(this.props.value)} >{this.props.value}</button>
			</div>
		)
	}
}