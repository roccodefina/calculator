import React from 'react';
import './style.scss'

export default class Screen extends React.Component {
	constructor(props){
		super(props)
	}

	render(){
		return(
			<div className="col-12 display">
				<div className="history" id="changingDisplay">{this.props.displayCount}</div>
				<div className="result" >{this.props.result}</div>
			</div>
			
		)
	}

}