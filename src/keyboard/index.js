import React from 'react';
import Button from '../button';
import './style.scss';

export default class Keyboard extends React.Component {
	constructor(props){
		super(props)
		
	}

	render(){
		return(
			<div className="row">
				<div className="col-3 buttonscenter">
					<Button value="MC" type="buttonstyle buttongrey"/>
					<Button value="(" type="buttonstyle buttonblack" inputDigit={this.props.inputDigit}/>
					<Button value="7" type="buttonstyle buttonblack" inputDigit={this.props.inputDigit}/>
					<Button value="4" type="buttonstyle buttonblack" inputDigit={this.props.inputDigit}/>
					<Button value="1" type="buttonstyle buttonblack" inputDigit={this.props.inputDigit}/>
					<Button value="0" type="buttonstyle buttonblack" inputDigit={this.props.inputDigit}/>														
				</div>
				<div className="col-3 buttonscenter">
					<Button value="MD" type="buttonstyle buttongrey"/>
					<Button value=")" type="buttonstyle buttonblack" inputDigit={this.props.inputDigit}/>
					<Button value="8" type="buttonstyle buttonblack" inputDigit={this.props.inputDigit}/>
					<Button value="5" type="buttonstyle buttonblack" inputDigit={this.props.inputDigit}/>
					<Button value="2" type="buttonstyle buttonblack" inputDigit={this.props.inputDigit}/>
					<Button value="." type="buttonstyle buttonblack" inputDigit={this.props.inputDigit}/>												
				</div>
				<div className="col-3 buttonscenter">
					<Button value="MS" type="buttonstyle buttongrey"/>
					<Button value="DE" type="buttonstyle buttonblack" inputDigit={this.props.inputDigit}/>
					<Button value="9" type="buttonstyle buttonblack" inputDigit={this.props.inputDigit}/>
					<Button value="6" type="buttonstyle buttonblack" inputDigit={this.props.inputDigit}/>
					<Button value="3" type="buttonstyle buttonblack" inputDigit={this.props.inputDigit}/>
					<Button value="C" type="buttonstyle buttonblack"  inputDigit={this.props.inputDigit}/>					
				</div>
				<div className="col-3 buttonscenter">
					<Button value="M+" type="buttonstyle buttongrey"/>	
					<Button value="÷" type="buttonstyle buttonpink"  inputDigit={this.props.inputDigit}/>
					<Button value="x" type="buttonstyle buttonpink"  inputDigit={this.props.inputDigit}/>
					<Button value="-" type="buttonstyle buttonpink"  inputDigit={this.props.inputDigit}/>
					<Button value="+" type="buttonstyle buttonpink"  inputDigit={this.props.inputDigit}/>
					<Button value="=" type="buttonstyle buttonorange" inputDigit={this.props.inputDigit}/>										
				</div>
			</div>
		)
	}
}